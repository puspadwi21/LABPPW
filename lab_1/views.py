from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Dwipuspa Ramadhanti Santoso' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,12,22) #TODO Implement this, format (Year, Month, Date)
npm = 1706043374 # TODO Implement this
almamater = 'Universitas Indonesia'
hobby = 'Singing along loudly to my Spotify playlist inside my kamar kostan'
description = 'A student pursuing a degree in Information System, soon going to be in her 20s'
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'almamater': almamater, 'hobby': hobby, 'description': description}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
